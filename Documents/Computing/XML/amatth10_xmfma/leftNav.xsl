<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="xs"
    version="2.0">
    <xsl:template match="/">
            <div id="leftnavcontainer">
                <ul id="leftlist">
                    <xsl:call-template name="list-item"/>             
                </ul>
            </div>       
    </xsl:template>
    
    <xsl:key name="x" match="type" use="." />
    <xsl:template name ="list-item" match="/root">
        <xsl:for-each select="/products/product">
            <xsl:if test="generate-id(type) = generate-id(key('x', type)[1])">
                <li>
                    <xsl:element name="a">
                        <xsl:attribute name="class">leftnavitem</xsl:attribute>
                        <xsl:attribute name="data-juice-type"><xsl:value-of select="type"/></xsl:attribute>
                        <xsl:attribute name="href">#</xsl:attribute>
                        <xsl:value-of select="type"/> Juices
                    </xsl:element>
                </li>
            </xsl:if>
        </xsl:for-each>
    </xsl:template>
</xsl:stylesheet>