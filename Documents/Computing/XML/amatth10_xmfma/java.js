function loadXMLDocument(fname)
{
if (window.ActiveXObject)
  {
  xhttp = new ActiveXObject("Msxml2.XMLHTTP");
  }
else
  {
  xhttp = new XMLHttpRequest();
  }
xhttp.open("GET", fname, false);
try {xhttp.responseType = "msxml-document"} catch(err) {} // Helps IE11
xhttp.send("");
return xhttp.responseXML;
}

function init()
{
    renderNav("leftNav.xsl", "leftcol")
    renderProducts("juice.xsl", "product", null)
    
    var list = document.getElementsByClassName('leftnavitem');
    for(var i=0; i<list.length; i++){
        var el = list[i];   
        el.onclick = (function(){
            var productType = this.getAttribute("data-juice-type");
            renderProducts("juice.xsl", "product", productType);
            toggleNavSelected(this);
        });     
    }
    refreshInfoOnClick();     
}
function renderNav(xslFile, divId)
{
xml = loadXMLDocument("products.xml");
xsl = loadXMLDocument(xslFile);

// code for IE
if (window.ActiveXObject || xhttp.responseType == "msxml-document")
  {
  product = xml.transformNode(xsl);
  document.getElementById(divId).innerHTML = product;
  }
// code for Chrome, Firefox, Opera, etc.
else if (document.implementation && document.implementation.createDocument)
  {
  xsltProcessor = new XSLTProcessor();
  xsltProcessor.importStylesheet(xsl);
  transDoc = xsltProcessor.transformToFragment(xml, document);
  document.getElementById(divId).appendChild(transDoc);
  }
}
function renderProducts(xslFile, divId, productType)
{
xml = loadXMLDocument("products.xml");
xsl = loadXMLDocument(xslFile);
// if no product has been clicked on load the first product type
if(productType==null){productType=document.getElementsByClassName('leftnavitem')[0].getAttribute("data-juice-type");}
// code for IE
if (window.ActiveXObject || xhttp.responseType == "msxml-document")
  {
  product = xml.transformNode(xsl);
  document.getElementById(divId).innerHTML = product;
  }
// code for Chrome, Firefox, Opera, etc.
else if (document.implementation && document.implementation.createDocument)
  {
  xsltProcessor = new XSLTProcessor();
  xsltProcessor.importStylesheet(xsl);
  xsltProcessor.setParameter(null,"typeSelected", productType);
  transDoc = xsltProcessor.transformToFragment(xml, document);
  document.getElementById(divId).innerHTML = '';
  document.getElementById(divId).appendChild(transDoc);
  }
  refreshInfoOnClick();
}

function refreshInfoOnClick(){
    var list = document.getElementsByClassName('info');
    for(var i=0; i<list.length; i++){
        var el = list[i];   
        el.onclick = (function(){
            renderNav("leftNav.xsl", "info");
        });           
    }
}

// insert current when one of the juice types is selected so that the colour of the clicked link changes
function toggleNavSelected(el){
    var list = document.getElementsByClassName('leftnavitem');
    for(var i=0; i<list.length; i++){
        var cur = list[i];
          if(el==cur){
            cur.classList.add("current");
            cur.firstChild = (function(){
                toggleNavSelected(this.parentElement);
                return false;
            });
        } else {
            if(cur.classList.contains("current")){
                cur.classList.remove("current");
            }
            cur.firstChild = (function(){
                toggleNavSelected(this.parentElement);
            });
        }
    }
}
