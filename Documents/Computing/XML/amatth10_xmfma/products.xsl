<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="xs"
    version="2.0">
    <xsl:output method="html"/>
    <xsl:template match="/">
        <html>
            <head>
                <title>Yum Juices</title>
                <link rel="stylesheet" type="text/css" href="yumstyle.css" />
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                <script type="text/javascript" src="java.js"></script>
            </head>
            <body>
                <div id="main">
                    <div id="commonlinksbar">
                        |  <a  href="#">about us</a> |
                        <a  href="#">contact us</a> |
                        <a  href="#">sitemap</a> |
                        <a  href="#">accessibility</a> |
                    </div>           
                    <div id="header">
                        <h1>Yum Juices</h1>
                    </div>
                    <div id="topnavcontainer">
                        <ul id="topnavbar">
                            <li><a href="index.html">Home</a></li>
                            <li><a class="current" href="#" >Juices</a></li>               
                            <li><a href="#">Juicers</a></li>
                            <li><a href="#">Recipes</a></li>
                            <li><a href="#">Promotions</a></li>
                        </ul>
                    </div>
                    <div id="contentcontainer">
                        <div id="rightcol">                   
                            <div id="breadcrumbbar">
                                <a class="small" href="index.html">Home</a> >
                                <a class="small" href="juices.html">Juices</a> >
                                <a class="small" href="fruitjuices.html">Fruit Juices</a> >
                                <a class="small" href="#">full XML file</a>
                            </div>
                            <div class="featureblock" >
                                <table border="1px" cellpadding="2px">    
                                    <xsl:apply-templates/>
                                </table>
                            </div>        
                        </div>               
                        <div id="leftcol">
                            <div id="leftnavcontainer">
                                <ul>
                                    <li><a href="fruitjuices.html">Back to our juices</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div id="footer">
                        <a href="fruitjuices.html">back</a>
                        
                        <p>Copyright &#169;Yum Juices, 2011</p>                            
                    </div>
                </div>
            </body>
        </html>
    </xsl:template>
    <xsl:template match="*[*][parent::*]">
        <tr>
            <td><xsl:value-of select="name()"/></td>
            <td>
                <table border="1px">
                    <xsl:apply-templates/>
                </table>
            </td>
        </tr>
    </xsl:template>
    
    <xsl:template match="*[not(*)]">
        <tr>
            <td><xsl:value-of select="name()"/></td>
            <td><xsl:value-of select="."/></td>
        </tr>
    </xsl:template>
</xsl:stylesheet>