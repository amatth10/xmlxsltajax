<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="xs"
    version="1.0">
    <xsl:key name="product-by-type" match="product" use="type" />
    <xsl:param name="typeSelected"/>
    
    <xsl:template match="/">        
        <xsl:for-each select="key('product-by-type', $typeSelected)">
            <div class="productblock">
                <xsl:element name="img">
                    <xsl:attribute name="class">juice</xsl:attribute>
                    <xsl:attribute name="src">
                        <xsl:value-of select="imageurl"/>
                    </xsl:attribute>
                    <xsl:attribute name="width">70</xsl:attribute>
                    <xsl:attribute name="height">70</xsl:attribute>
                    <xsl:attribute name="alt">juice</xsl:attribute>            
                </xsl:element>
                <h3>
                    <xsl:value-of select="name"/>
                </h3>
                <p>
                    <xsl:value-of select="fruitinfo"/> 
                    <span class="highlight"><xsl:text>&#xa;</xsl:text>
                        <xsl:value-of select="name"/> -  
                    </span><xsl:value-of select="juiceinfo"/>
                </p>
                <xsl:element name="div">
                    <xsl:attribute name="id">
                        <xsl:value-of select="name"/> 
                    </xsl:attribute>
                </xsl:element>
                <p><a class="info" href="products.xml">Juices data sheet</a></p>
            </div>
        </xsl:for-each>
    </xsl:template>
</xsl:stylesheet>